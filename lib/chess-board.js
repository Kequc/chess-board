var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Chess;
(function (Chess) {
    var Board = (function () {
        function Board(element) {
            var _this = this;
            this.isReversed = false;
            this.isOpponentTurn = false;
            this.pieces = [];
            this.element = element;
            this.promotion = new Chess.Promotion(this);
            this.squares = Chess.Forge.createSquares();
            var _loop_1 = function(i) {
                this_1.squares[i].addEventListener('dragenter', function () {
                    Chess.Controls.dragEnter(_this, i);
                });
                this_1.element.appendChild(this_1.squares[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.squares.length; i++) {
                _loop_1(i);
            }
            window.addEventListener('resize', this._resizePieces.bind(this));
        }
        Board.prototype.start = function (isReversed, arrangement) {
            this.history = new Chess.History();
            this.isReversed = isReversed || false;
            arrangement = arrangement || (this.isReversed ? 'rnbkqbnrpppppppp8888PPPPPPPPRNBKQBNR' : 'rnbqkbnrpppppppp8888PPPPPPPPRNBQKBNR');
            var pieces = Chess.Forge.createPieces(this, arrangement);
            for (var i = 0; i < pieces.length; i++) {
                var piece = pieces[i];
                if (piece) {
                    piece.setPosition(i, true);
                    this.pieces.push(piece);
                }
            }
            this._resizePieces();
            this.setTurn(this.isReversed);
        };
        Board.prototype.setTurn = function (isOpponent) {
            if (isOpponent === void 0) { isOpponent = !this.isOpponentTurn; }
            this.isOpponentTurn = isOpponent;
            this.element.classList.toggle('is-opponent-turn', isOpponent);
            for (var _i = 0, _a = this.squares; _i < _a.length; _i++) {
                var square = _a[_i];
                square.classList.remove('is-contested');
            }
            var king = Chess.State.findKing(this, isOpponent);
            var canMove = Chess.State.isMoveAvailable(this, isOpponent);
            if (Chess.State.isCheck(this, isOpponent)) {
                this.squares[king.position].classList.add('is-contested');
                if (!canMove)
                    this.checkmate(isOpponent);
            }
            else if (!canMove)
                this.stalemate(isOpponent);
        };
        Board.prototype.undo = function () {
            var move = this.history.remove();
            if (!move)
                return;
            for (var _i = 0, _a = move.changes; _i < _a.length; _i++) {
                var change = _a[_i];
                change.piece.setPosition(change.start);
            }
            this.setTurn(move.changes[0].piece.isOpponent);
        };
        Board.prototype.checkmate = function (isOpponent) {
            console.log('game over!');
            console.log('checkmate');
        };
        Board.prototype.stalemate = function (isOpponent) {
            console.log('game over!');
            console.log('stalemate');
        };
        Board.prototype._resizePieces = function () {
            for (var _i = 0, _a = this.pieces; _i < _a.length; _i++) {
                var piece = _a[_i];
                piece.resize();
            }
        };
        return Board;
    }());
    Chess.Board = Board;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    function gridIndex(row, col) {
        if (row >= 0 && row <= 7 && col >= 0 && col <= 7)
            return (row * 8) + col;
    }
    Chess.gridIndex = gridIndex;
    function gridIndexOffset(index, r, c) {
        var row = gridRow(index);
        var col = gridCol(index);
        return gridIndex(row + r, col + c);
    }
    Chess.gridIndexOffset = gridIndexOffset;
    function gridIndexDirection(index, direction, dist) {
        if (dist === void 0) { dist = 1; }
        var row = gridRow(index);
        var col = gridCol(index);
        switch (direction) {
            case 1: return gridIndex(row - dist, col - dist);
            case 2: return gridIndex(row - dist, col);
            case 3: return gridIndex(row - dist, col + dist);
            case 4: return gridIndex(row, col + dist);
            case 5: return gridIndex(row + dist, col + dist);
            case 6: return gridIndex(row + dist, col);
            case 7: return gridIndex(row + dist, col - dist);
            case 8: return gridIndex(row, col - dist);
        }
        return gridIndex(row, col);
    }
    Chess.gridIndexDirection = gridIndexDirection;
    function gridRow(index) {
        return Math.floor(index / 8);
    }
    Chess.gridRow = gridRow;
    function gridCol(index) {
        return index % 8;
    }
    Chess.gridCol = gridCol;
})(Chess || (Chess = {}));
window.addEventListener('DOMContentLoaded', function () {
    var element = document.querySelector('.chess-board');
    if (element) {
        window.board = new Chess.Board(element);
        window.board.start();
    }
});
var Chess;
(function (Chess) {
    var Controls;
    (function (Controls) {
        var _selected;
        var _validMoves;
        var _dragIndex;
        function unhover(board) {
            if (_dragIndex != null)
                board.squares[_dragIndex].classList.remove('is-hovered');
            _dragIndex = undefined;
        }
        Controls.unhover = unhover;
        function dragStart(piece) {
            if (!piece)
                return;
            piece.board.element.classList.add('is-dragging');
            piece.board.squares[piece.position].classList.add('is-start');
            _validMoves = piece.validMoves();
            for (var i = 0; i < piece.board.squares.length; i++) {
                var square = piece.board.squares[i];
                var isValid = _validMoves.indexOf(i) > -1;
                square.classList.toggle('is-valid', isValid);
            }
            _selected = piece;
        }
        Controls.dragStart = dragStart;
        function dragEnd(board) {
            board.element.classList.remove('is-dragging');
            if (_selected) {
                var square = board.squares[_selected.position];
                if (square)
                    square.classList.remove('is-start');
                if (_validMoves.indexOf(_dragIndex) > -1) {
                    _selected.move(_dragIndex);
                    _selected.board.setTurn();
                }
            }
            unhover(board);
            _selected = undefined;
            _validMoves = undefined;
        }
        Controls.dragEnd = dragEnd;
        function dragEnter(board, index) {
            unhover(board);
            _dragIndex = index;
            board.squares[_dragIndex].classList.add('is-hovered');
        }
        Controls.dragEnter = dragEnter;
    })(Controls = Chess.Controls || (Chess.Controls = {}));
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Forge;
    (function (Forge) {
        function createPieces(board, arrangement) {
            var result = [];
            var index = 0;
            for (var _i = 0, _a = arrangement.split(''); _i < _a.length; _i++) {
                var p = _a[_i];
                if (!!parseInt(p))
                    index += parseInt(p);
                else if (index < 64) {
                    switch (p) {
                        case 'p':
                            result[index] = new Chess.Pawn(board, true);
                            break;
                        case 'r':
                            result[index] = new Chess.Rook(board, true);
                            break;
                        case 'n':
                            result[index] = new Chess.Knight(board, true);
                            break;
                        case 'b':
                            result[index] = new Chess.Bishop(board, true);
                            break;
                        case 'q':
                            result[index] = new Chess.Queen(board, true);
                            break;
                        case 'k':
                            result[index] = new Chess.King(board, true);
                            break;
                        case 'P':
                            result[index] = new Chess.Pawn(board, false);
                            break;
                        case 'R':
                            result[index] = new Chess.Rook(board, false);
                            break;
                        case 'N':
                            result[index] = new Chess.Knight(board, false);
                            break;
                        case 'B':
                            result[index] = new Chess.Bishop(board, false);
                            break;
                        case 'Q':
                            result[index] = new Chess.Queen(board, false);
                            break;
                        case 'K':
                            result[index] = new Chess.King(board, false);
                            break;
                    }
                    index++;
                }
            }
            return result;
        }
        Forge.createPieces = createPieces;
        function createSquares() {
            var result = [];
            for (var row = 0; row < 8; row++) {
                for (var col = 0; col < 8; col++) {
                    var square = document.createElement('div');
                    var index = Chess.gridIndex(row, col);
                    square.classList.add('square');
                    if (row % 2 == col % 2)
                        square.classList.add('is-light');
                    result.push(square);
                }
            }
            return result;
        }
        Forge.createSquares = createSquares;
    })(Forge = Chess.Forge || (Chess.Forge = {}));
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var History = (function () {
        function History() {
            this._moves = [];
        }
        History.prototype.last = function () {
            return this._moves[this._moves.length - 1];
        };
        History.prototype.remove = function () {
            return this._moves.pop();
        };
        History.prototype.add = function (item) {
            this._moves.push(item);
        };
        return History;
    }());
    Chess.History = History;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Piece = (function () {
        function Piece(board, isOpponent) {
            this._isReady = false;
            this._isMoved = false;
            this.position = -1;
            this.board = board;
            this.isOpponent = isOpponent;
            this._buildElement();
        }
        Piece.prototype.isMoved = function () {
            return this._isMoved;
        };
        Piece.prototype.resize = function () {
            var height = this.board.squares[0].offsetHeight;
            this.element.style.lineHeight = height - 6 + 'px';
            this.element.style.fontSize = (height * 0.6) + 'px';
        };
        Piece.prototype.move = function (position, changes) {
            if (changes === void 0) { changes = []; }
            var taken = Chess.State.pieceAt(this.board, position);
            if (taken) {
                changes.unshift({
                    start: taken.position,
                    stop: -1,
                    piece: taken,
                    kind: 'taken'
                });
                taken.remove();
            }
            changes.unshift({
                start: this.position,
                stop: position,
                piece: this
            });
            this.setPosition(position);
            this.board.history.add({
                changes: changes,
                time: Date.now()
            });
        };
        Piece.prototype.setVisiblePosition = function (position) {
            if (position > -1)
                this.board.squares[position].appendChild(this.element);
            else if (this.element.parentElement)
                this.element.parentElement.removeChild(this.element);
        };
        Piece.prototype.setPosition = function (position, force) {
            if (force === void 0) { force = false; }
            if (position <= -1)
                this.remove();
            else {
                this._addEventListeners();
                this.position = position;
                this.setVisiblePosition(this.position);
            }
            if (!force)
                this._isMoved = true;
        };
        Piece.prototype.remove = function () {
            this.position = -1;
            this.setVisiblePosition(this.position);
            this._isMoved = true;
        };
        Piece.prototype.validMoves = function () {
            var _this = this;
            return this.validPositions().filter(function (position) {
                return _this.isValidMove(position);
            });
        };
        Piece.prototype.isValidMove = function (position) {
            var taken = Chess.State.pieceAt(this.board, position);
            var takenPosition;
            if (taken) {
                takenPosition = taken.position;
                taken.position = -1;
            }
            var piecePosition = this.position;
            this.position = position;
            var inCheck = Chess.State.isCheck(this.board, this.isOpponent);
            if (taken)
                taken.position = takenPosition;
            this.position = piecePosition;
            return !inCheck;
        };
        Piece.prototype.isWhite = function () {
            return (!this.isOpponent && !this.board.isReversed) || (this.isOpponent && this.board.isReversed);
        };
        Piece.prototype._buildElement = function () {
            if (this.element)
                return;
            var element = document.createElement('div');
            element.classList.add('piece');
            element.classList.toggle('is-opponent', this.isOpponent);
            element.classList.toggle('is-white', this.isWhite());
            element.appendChild(document.createTextNode(this.hex()));
            this.element = element;
            this.resize();
        };
        Piece.prototype._addEventListeners = function () {
            var _this = this;
            if (this._isReady)
                return;
            this._isReady = true;
            this.board.pieces.push(this);
            this.element.addEventListener('dragstart', function () {
                Chess.Controls.dragStart(_this);
            });
            this.element.addEventListener('dragend', function () {
                Chess.Controls.dragEnd(_this.board);
            });
            this.element.setAttribute('draggable', 'true');
        };
        return Piece;
    }());
    Chess.Piece = Piece;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Promotion = (function () {
        function Promotion(board) {
            this.board = board;
        }
        Promotion.prototype.show = function (isOpponent, callback) {
            this._buildElement();
            this._clear();
            this._addChoice(new Chess.Queen(this.board, isOpponent), callback);
            this._addChoice(new Chess.Bishop(this.board, isOpponent), callback);
            this._addChoice(new Chess.Knight(this.board, isOpponent), callback);
            this._addChoice(new Chess.Rook(this.board, isOpponent), callback);
            this.element.classList.remove('is-hidden');
        };
        Promotion.prototype._addChoice = function (piece, callback) {
            var _this = this;
            var element = document.createElement('div');
            element.classList.add('choice');
            element.appendChild(piece.element);
            element.addEventListener('click', function () {
                callback(piece);
                _this.hide();
            });
            this.element.appendChild(element);
        };
        Promotion.prototype.hide = function () {
            if (!this.element)
                return;
            this.element.classList.add('is-hidden');
            this._clear();
        };
        Promotion.prototype._clear = function () {
            while (this.element.firstChild) {
                this.element.removeChild(this.element.firstChild);
            }
        };
        Promotion.prototype._buildElement = function () {
            if (this.element)
                return;
            this.element = document.createElement('div');
            this.element.classList.add('promotion', 'is-hidden');
            this.board.element.appendChild(this.element);
        };
        return Promotion;
    }());
    Chess.Promotion = Promotion;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var State;
    (function (State) {
        function validDirectionFull(piece, direction) {
            var result = [];
            while (true) {
                var index = validDirection(piece, direction, result.length + 1);
                if (index == null)
                    break;
                result.push(index);
                if (pieceAt(piece.board, index))
                    break;
            }
            return result;
        }
        State.validDirectionFull = validDirectionFull;
        function validDirection(piece, direction, dist) {
            if (dist === void 0) { dist = 1; }
            var index = Chess.gridIndexDirection(piece.position, direction, dist);
            return valid(piece, index);
        }
        State.validDirection = validDirection;
        function validOffset(piece, row, col) {
            var index = Chess.gridIndexOffset(piece.position, row, col);
            return valid(piece, index);
        }
        State.validOffset = validOffset;
        function valid(piece, position) {
            if (position == null)
                return;
            var occupied = pieceAt(piece.board, position);
            if (!occupied || occupied.isOpponent != piece.isOpponent)
                return position;
        }
        State.valid = valid;
        function isMoveAvailable(board, isOpponent) {
            for (var _i = 0, _a = board.pieces; _i < _a.length; _i++) {
                var piece = _a[_i];
                if (piece.isOpponent == isOpponent) {
                    if (piece.validMoves().length > 0)
                        return true;
                }
            }
            return false;
        }
        State.isMoveAvailable = isMoveAvailable;
        function isCheck(board, isOpponent) {
            var king = findKing(board, isOpponent);
            for (var _i = 0, _a = board.pieces; _i < _a.length; _i++) {
                var piece = _a[_i];
                if (piece.isOpponent != isOpponent) {
                    if (piece.validPositions().indexOf(king.position) > -1)
                        return true;
                }
            }
            return false;
        }
        State.isCheck = isCheck;
        function pieceAt(board, position) {
            for (var _i = 0, _a = board.pieces; _i < _a.length; _i++) {
                var piece = _a[_i];
                if (piece.position == position)
                    return piece;
            }
        }
        State.pieceAt = pieceAt;
        function findKing(board, isOpponent) {
            for (var _i = 0, _a = board.pieces; _i < _a.length; _i++) {
                var piece = _a[_i];
                if (piece.isOpponent == isOpponent && piece instanceof Chess.King)
                    return piece;
            }
        }
        State.findKing = findKing;
    })(State = Chess.State || (Chess.State = {}));
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Bishop = (function (_super) {
        __extends(Bishop, _super);
        function Bishop() {
            _super.apply(this, arguments);
        }
        Bishop.prototype.hex = function () {
            return (this.isWhite() ? '\u2657' : '\u265D');
        };
        Bishop.prototype.value = function () {
            return 3;
        };
        Bishop.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var directions = [1, 3, 5, 7];
            for (var _i = 0, directions_1 = directions; _i < directions_1.length; _i++) {
                var direction = directions_1[_i];
                result = result.concat(Chess.State.validDirectionFull(this, direction));
            }
            return result;
        };
        return Bishop;
    }(Chess.Piece));
    Chess.Bishop = Bishop;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var King = (function (_super) {
        __extends(King, _super);
        function King() {
            _super.apply(this, arguments);
        }
        King.prototype.hex = function () {
            return (this.isWhite() ? '\u2654' : '\u265A');
        };
        King.prototype.value = function () {
            return 0;
        };
        King.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var directions = [1, 2, 3, 4, 5, 6, 7, 8];
            var position;
            for (var _i = 0, directions_2 = directions; _i < directions_2.length; _i++) {
                var direction = directions_2[_i];
                position = Chess.State.validDirection(this, direction);
                if (position != null)
                    result.push(position);
            }
            position = this._validCastle(8);
            if (position != null)
                result.push(position);
            position = this._validCastle(4);
            if (position != null)
                result.push(position);
            return result;
        };
        King.prototype.move = function (position) {
            var col = Chess.gridCol(position) - Chess.gridCol(this.position);
            if (Math.abs(col) == 2) {
                var direction = (col < 0 ? 8 : 4);
                var castle = this._castle(direction);
                if (castle) {
                    direction = (col > 0 ? 8 : 4);
                    var stop = Chess.gridIndexDirection(position, direction);
                    var changes = [{
                            start: castle.position,
                            stop: stop,
                            piece: castle,
                            kind: 'castle'
                        }];
                    castle.setPosition(stop);
                    _super.prototype.move.call(this, position, changes);
                    return;
                }
            }
            _super.prototype.move.call(this, position);
        };
        King.prototype.validMoves = function () {
            var _this = this;
            var col = Chess.gridCol(this.position);
            return this.validPositions().filter(function (position) {
                var c = Chess.gridCol(position);
                if (Math.abs(c - col) == 2) {
                    if (Chess.State.isCheck(_this.board, _this.isOpponent))
                        return false;
                    var castle = Chess.gridIndexOffset(_this.position, 0, (c - col) / 2);
                    if (!_this.isValidMove(castle))
                        return false;
                }
                return _this.isValidMove(position);
            });
        };
        King.prototype._castle = function (direction) {
            if (this.isMoved())
                return;
            var col;
            switch (direction) {
                case 8:
                    col = (this.board.isReversed ? -3 : -4);
                    break;
                case 4:
                    col = (this.board.isReversed ? 4 : 3);
                    break;
                default: return;
            }
            var position = Chess.gridIndexOffset(this.position, 0, col);
            var rook = Chess.State.pieceAt(this.board, position);
            if (rook && !rook.isMoved())
                return rook;
        };
        King.prototype._validCastle = function (direction) {
            var rook = this._castle(direction);
            if (!rook)
                return;
            var cols;
            switch (direction) {
                case 8:
                    cols = (this.board.isReversed ? [-1, -2] : [-1, -2, -3]);
                    break;
                case 4:
                    cols = (this.board.isReversed ? [1, 2, 3] : [1, 2]);
                    break;
                default: return;
            }
            for (var _i = 0, cols_1 = cols; _i < cols_1.length; _i++) {
                var col = cols_1[_i];
                var position = Chess.gridIndexOffset(this.position, 0, col);
                if (Chess.State.pieceAt(this.board, position))
                    return;
            }
            return Chess.gridIndexDirection(this.position, direction, 2);
        };
        return King;
    }(Chess.Piece));
    Chess.King = King;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Knight = (function (_super) {
        __extends(Knight, _super);
        function Knight() {
            _super.apply(this, arguments);
        }
        Knight.prototype.hex = function () {
            return (this.isWhite() ? '\u2658' : '\u265E');
        };
        Knight.prototype.value = function () {
            return 3;
        };
        Knight.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var offsets = [[-2, -1], [-2, 1], [-1, 2], [1, 2], [2, -1], [2, 1], [-1, -2], [1, -2]];
            for (var _i = 0, offsets_1 = offsets; _i < offsets_1.length; _i++) {
                var offset = offsets_1[_i];
                var index = Chess.State.validOffset(this, offset[0], offset[1]);
                if (index != null)
                    result.push(index);
            }
            return result;
        };
        return Knight;
    }(Chess.Piece));
    Chess.Knight = Knight;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Pawn = (function (_super) {
        __extends(Pawn, _super);
        function Pawn() {
            _super.apply(this, arguments);
        }
        Pawn.prototype.hex = function () {
            return (this.isWhite() ? '\u2659' : '\u265F');
        };
        Pawn.prototype.value = function () {
            return 1;
        };
        Pawn.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var direction = (this.isOpponent ? 6 : 2);
            var position = Chess.gridIndexDirection(this.position, direction);
            if (position != null && !Chess.State.pieceAt(this.board, position)) {
                result.push(position);
                if (!this.isMoved()) {
                    position = Chess.gridIndexDirection(this.position, direction, 2);
                    if (position != null && !Chess.State.pieceAt(this.board, position))
                        result.push(position);
                }
            }
            position = this._validCapture(-1);
            if (position != null)
                result.push(position);
            position = this._validCapture(1);
            if (position != null)
                result.push(position);
            return result;
        };
        Pawn.prototype.move = function (position) {
            var row = Chess.gridRow(position);
            if (row == 0 || row == 7) {
                this._performPromotion(position);
                return;
            }
            var col = Chess.gridCol(position) - Chess.gridCol(this.position);
            if (Math.abs(col) == 1) {
                var passant = this._passant(col);
                if (passant) {
                    var changes = [{
                            start: passant.position,
                            stop: -1,
                            piece: passant,
                            kind: 'passant'
                        }];
                    passant.remove();
                    _super.prototype.move.call(this, position, changes);
                    return;
                }
            }
            _super.prototype.move.call(this, position);
        };
        Pawn.prototype._performPromotion = function (position) {
            var _this = this;
            var taken = Chess.State.pieceAt(this.board, position);
            if (taken)
                taken.setVisiblePosition(-1);
            this.setVisiblePosition(position);
            this.board.promotion.show(this.isOpponent, function (promoted) {
                var changes = [{
                        start: _this.position,
                        stop: -1,
                        piece: _this,
                        kind: 'promoted'
                    }];
                _this.remove();
                promoted.move(position, changes);
            });
        };
        Pawn.prototype._passant = function (col) {
            var lastMove = this.board.history.last();
            if (!lastMove || !(lastMove.changes[0].piece instanceof Pawn) || lastMove.changes[0].piece.isOpponent == this.isOpponent)
                return;
            if (Math.abs(Chess.gridRow(lastMove.changes[0].start) - Chess.gridRow(lastMove.changes[0].stop)) == 2) {
                var position = Chess.gridIndexOffset(this.position, 0, col);
                var occupied = Chess.State.pieceAt(this.board, position);
                if (occupied && lastMove.changes[0].piece == occupied)
                    return occupied;
            }
        };
        Pawn.prototype._validCapture = function (col) {
            var row = (this.isOpponent ? 1 : -1);
            var position = Chess.gridIndexOffset(this.position, row, col);
            var occupied = Chess.State.pieceAt(this.board, position);
            if (occupied && occupied.isOpponent != this.isOpponent)
                return position;
            var passant = this._passant(col);
            if (passant)
                return position;
        };
        return Pawn;
    }(Chess.Piece));
    Chess.Pawn = Pawn;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Queen = (function (_super) {
        __extends(Queen, _super);
        function Queen() {
            _super.apply(this, arguments);
        }
        Queen.prototype.hex = function () {
            return (this.isWhite() ? '\u2655' : '\u265B');
        };
        Queen.prototype.value = function () {
            return 9;
        };
        Queen.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var directions = [1, 2, 3, 4, 5, 6, 7, 8];
            for (var _i = 0, directions_3 = directions; _i < directions_3.length; _i++) {
                var direction = directions_3[_i];
                result = result.concat(Chess.State.validDirectionFull(this, direction));
            }
            return result;
        };
        return Queen;
    }(Chess.Piece));
    Chess.Queen = Queen;
})(Chess || (Chess = {}));
var Chess;
(function (Chess) {
    var Rook = (function (_super) {
        __extends(Rook, _super);
        function Rook() {
            _super.apply(this, arguments);
        }
        Rook.prototype.hex = function () {
            return (this.isWhite() ? '\u2656' : '\u265C');
        };
        Rook.prototype.value = function () {
            return 5;
        };
        Rook.prototype.validPositions = function () {
            if (this.position == -1)
                return [];
            var result = [];
            var directions = [2, 4, 6, 8];
            for (var _i = 0, directions_4 = directions; _i < directions_4.length; _i++) {
                var direction = directions_4[_i];
                result = result.concat(Chess.State.validDirectionFull(this, direction));
            }
            return result;
        };
        return Rook;
    }(Chess.Piece));
    Chess.Rook = Rook;
})(Chess || (Chess = {}));
