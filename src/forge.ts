namespace Chess.Forge
{
    export function createPieces (board: Board, arrangement: string): Piece[]
    {
        // create pieces for game board
        let result: Piece[] = [];
        let index = 0;

        for (let p of arrangement.split('')) {
            if (!!parseInt(p))
                index += parseInt(p);
            else if (index < 64) {
                switch (p) {
                    case 'p': result[index] = new Pawn(board, true); break;
                    case 'r': result[index] = new Rook(board, true); break;
                    case 'n': result[index] = new Knight(board, true); break;
                    case 'b': result[index] = new Bishop(board, true); break;
                    case 'q': result[index] = new Queen(board, true); break;
                    case 'k': result[index] = new King(board, true); break;
                    case 'P': result[index] = new Pawn(board, false); break;
                    case 'R': result[index] = new Rook(board, false); break;
                    case 'N': result[index] = new Knight(board, false); break;
                    case 'B': result[index] = new Bishop(board, false); break;
                    case 'Q': result[index] = new Queen(board, false); break;
                    case 'K': result[index] = new King(board, false); break;
                }
                index++;
            }
        }

        return result;
    }

    export function createSquares (): HTMLElement[]
    {
        // create squares for game board
        let result: HTMLElement[] = [];

        for (let row = 0; row < 8; row++) {
            for (let col = 0; col < 8; col++) {
                let square = document.createElement('div');
                let index = gridIndex(row, col);

                square.classList.add('square');
                if (row % 2 == col % 2)
                    square.classList.add('is-light');
                
                result.push(square);
            }
        }

        return result;
    }
}
