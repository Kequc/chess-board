namespace Chess.State
{
    export function validDirectionFull (piece: Piece, direction: Direction): number[]
    {
        // all valid squares in a specified direction
        let result: number[] = [];

        while (true) {
            let index = validDirection(piece, direction, result.length + 1);
            if (index == null) break;
            result.push(index);
            if (pieceAt(piece.board, index)) break;
        }

        return result;
    }

    export function validDirection (piece: Piece, direction: Direction, dist: number = 1): number
    {
        // valid square in a specified direction and distance
        let index = gridIndexDirection(piece.position, direction, dist);
        return valid(piece, index);
    }

    export function validOffset (piece: Piece, row: number, col: number): number
    {
        // valid square at a specific offset
        let index = gridIndexOffset(piece.position, row, col);
        return valid(piece, index);
    }

    export function valid (piece: Piece, position: number): number
    {
        // space can be stood on
        if (position == null)
            return;
        let occupied = pieceAt(piece.board, position);
        if (!occupied || occupied.isOpponent != piece.isOpponent)
            return position;
    }

    export function isMoveAvailable (board: Board, isOpponent: boolean): boolean
    {
        // if player can move
        for (let piece of board.pieces) {
            if (piece.isOpponent == isOpponent) {
                if (piece.validMoves().length > 0)
                    return true;
            }
        }

        return false;
    }

    export function isCheck (board: Board, isOpponent: boolean): boolean
    {
        // if the board is currently in a check state
        let king = findKing(board, isOpponent);

        for (let piece of board.pieces) {
            if (piece.isOpponent != isOpponent) {
                if (piece.validPositions().indexOf(king.position) > -1)
                    return true;
            }
        }

        return false;
    }

    export function pieceAt (board: Board, position: number): Piece
    {
        // find piece on a specified square
        for (let piece of board.pieces) {
            if (piece.position == position)
                return piece;
        }
    }

    export function findKing (board: Board, isOpponent: boolean): King
    {
        // find king for specified player
        for (let piece of board.pieces) {
            if (piece.isOpponent == isOpponent && piece instanceof King)
                return piece;
        }
    }
}
