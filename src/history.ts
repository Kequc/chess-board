namespace Chess
{
    export interface IChange
    {
        start: number;
        stop: number;
        piece: Piece;
        kind?: string;
    }

    export interface IMove
    {
        changes: IChange[];
        time: number;
    }

    export class History
    {
        private _moves: IMove[] = [];

        last (): IMove
        {
            return this._moves[this._moves.length - 1];
        }

        remove (): IMove
        {
            return this._moves.pop();
        }

        add (item: IMove)
        {
            this._moves.push(item);
        }
    }
}
