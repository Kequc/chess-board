namespace Chess
{
    export const enum Direction { NONE, TL, T, TR, R, BR, B, BL, L }

    export function gridIndex (row: number, col: number): number
    {
        if (row >= 0 && row <= 7 && col >= 0 && col <= 7)
            return (row * 8) + col;
    }

    export function gridIndexOffset (index: number, r: number, c: number): number
    {
        let row = gridRow(index);
        let col = gridCol(index);
        return gridIndex(row + r, col + c);
    }

    export function gridIndexDirection (index: number, direction: Direction, dist: number = 1): number
    {
        let row = gridRow(index);
        let col = gridCol(index);
        switch (direction) {
            case Direction.TL: return gridIndex(row - dist, col - dist);
            case Direction.T: return gridIndex(row - dist, col);
            case Direction.TR: return gridIndex(row - dist, col + dist);
            case Direction.R: return gridIndex(row, col + dist);
            case Direction.BR: return gridIndex(row + dist, col + dist);
            case Direction.B: return gridIndex(row + dist, col);
            case Direction.BL: return gridIndex(row + dist, col - dist);
            case Direction.L: return gridIndex(row, col - dist);
        }
        return gridIndex(row, col);
    }

    export function gridRow (index: number): number
    {
        return Math.floor(index / 8);
    }

    export function gridCol (index: number): number
    {
        return index % 8;
    }
}

window.addEventListener('DOMContentLoaded', () => {
    let element = <HTMLElement>document.querySelector('.chess-board');
    if (element) {
        (<any>window).board = new Chess.Board(element);
        (<any>window).board.start();
    }
});
