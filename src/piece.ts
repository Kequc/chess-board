namespace Chess
{
    export abstract class Piece
    {
        private _isReady: boolean = false;
        private _isMoved: boolean = false;

        element: HTMLElement;

        position: number = -1;

        board: Board;
        isOpponent: boolean;

        constructor (board: Board, isOpponent: boolean)
        {
            this.board = board;
            this.isOpponent = isOpponent;
            this._buildElement();
        }

        abstract hex (): string;
        abstract value (): number;
        abstract validPositions (): number[]

        isMoved(): boolean
        {
            return this._isMoved;
        }

        resize ()
        {
            let height = this.board.squares[0].offsetHeight;

            this.element.style.lineHeight = height - 6 + 'px';
            this.element.style.fontSize = (height * 0.6) + 'px';
        }

        move (position: number, changes: IChange[] = [])
        {
            let taken = State.pieceAt(this.board, position);
            if (taken) {
                changes.unshift({
                    start: taken.position,
                    stop: -1,
                    piece: taken,
                    kind: 'taken'
                });
                taken.remove();
            }

            changes.unshift({
                start: this.position,
                stop: position,
                piece: this
            });

            this.setPosition(position);

            this.board.history.add({
                changes: changes,
                time: Date.now()
            });
        }

        setVisiblePosition (position: number)
        {
            if (position > -1)
                this.board.squares[position].appendChild(this.element);
            else if (this.element.parentElement)
                this.element.parentElement.removeChild(this.element);
        }

        setPosition (position: number, force: boolean = false)
        {
            if (position <= -1)
                this.remove();
            else {
                this._addEventListeners();
                this.position = position;
                this.setVisiblePosition(this.position);
            }

            if (!force)
                this._isMoved = true;
        }

        remove ()
        {
            this.position = -1;
            this.setVisiblePosition(this.position);
            this._isMoved = true;
        }

        validMoves (): number[]
        {
            // cache valid moves
            return this.validPositions().filter((position) => {
                return this.isValidMove(position);
            });
        }

        isValidMove (position: number): boolean
        {
            // moving this piece puts player in check?
            let taken = State.pieceAt(this.board, position);
            let takenPosition: number;
            if (taken) {
                takenPosition = taken.position;
                taken.position = -1;
            }

	        let piecePosition = this.position;
            this.position = position;

            let inCheck = State.isCheck(this.board, this.isOpponent);

            if (taken)
                taken.position = takenPosition;
            this.position = piecePosition;

            return !inCheck;
        }

        isWhite (): boolean
        {
            return (!this.isOpponent && !this.board.isReversed) || (this.isOpponent && this.board.isReversed);
        }

        private _buildElement ()
        {
            if (this.element)
                return;

            let element = document.createElement('div');
            element.classList.add('piece');
            element.classList.toggle('is-opponent', this.isOpponent);
            element.classList.toggle('is-white', this.isWhite());
            element.appendChild(document.createTextNode(this.hex()));
            this.element = element;
            this.resize();
        }

        private _addEventListeners ()
        {
            if (this._isReady)
                return;
            
            this._isReady = true;
            this.board.pieces.push(this);

            this.element.addEventListener('dragstart', () => {
                Controls.dragStart(this);
            });
            this.element.addEventListener('dragend', () => {
                Controls.dragEnd(this.board);
            });

            this.element.setAttribute('draggable', 'true');
        }
    }
}
