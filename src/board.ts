namespace Chess
{
    export class Board
    {
        element: HTMLElement;
        squares: HTMLElement[];

        promotion: Promotion;
        history: History;

        isReversed: boolean = false;
        isOpponentTurn: boolean = false;

        pieces: Piece[] = [];

        constructor (element: HTMLElement)
        {
            this.element = element;
            this.promotion = new Promotion(this);
            this.squares = Forge.createSquares();

            for (let i = 0; i < this.squares.length; i++) {
                this.squares[i].addEventListener('dragenter', () => {
                    Controls.dragEnter(this, i);
                });
                this.element.appendChild(this.squares[i]);
            }

            window.addEventListener('resize', this._resizePieces.bind(this));
        }

        start (isReversed?: boolean, arrangement?: string)
        {
            // start new game
            this.history = new History();

            this.isReversed = isReversed || false;
            arrangement = arrangement || (this.isReversed ? 'rnbkqbnrpppppppp8888PPPPPPPPRNBKQBNR' : 'rnbqkbnrpppppppp8888PPPPPPPPRNBQKBNR');

            let pieces = Forge.createPieces(this, arrangement);
            for (let i = 0; i < pieces.length; i++) {
                let piece = pieces[i];
                if (piece) {
                    piece.setPosition(i, true);
                    this.pieces.push(piece);
                }
            }

            this._resizePieces();

            this.setTurn(this.isReversed);
        }

        setTurn (isOpponent: boolean = !this.isOpponentTurn)
        {
            this.isOpponentTurn = isOpponent;
            this.element.classList.toggle('is-opponent-turn', isOpponent);

            for (let square of this.squares) {
                square.classList.remove('is-contested');
            }

            let king = State.findKing(this, isOpponent);
            let canMove = State.isMoveAvailable(this, isOpponent);

            if (State.isCheck(this, isOpponent)) {
                this.squares[king.position].classList.add('is-contested');

                if (!canMove)
                    this.checkmate(isOpponent); // GAME OVER
            }
            else if (!canMove)
                this.stalemate(isOpponent); // STALEMATE
        }

        undo ()
        {
            // undo last move
            let move = this.history.remove();
            if (!move)
                return;

            for (let change of move.changes) {
                change.piece.setPosition(change.start);
            }

            this.setTurn(move.changes[0].piece.isOpponent);
        }

        checkmate (isOpponent: boolean)
        {
            console.log('game over!');
            console.log('checkmate');
        }

        stalemate (isOpponent: boolean)
        {
            console.log('game over!');
            console.log('stalemate');
        }

        private _resizePieces ()
        {
            // resize pieces to fit game board
            for (let piece of this.pieces) {
                piece.resize();
            }
        }
    }
}
