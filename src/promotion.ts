namespace Chess
{
    export class Promotion
    {
        board: Board;
        element: HTMLElement;

        constructor (board: Board)
        {
            this.board = board;
        }

        show (isOpponent: boolean, callback: (promoted: Piece)=>any)
        {
            this._buildElement();
            this._clear();

            this._addChoice(new Queen(this.board, isOpponent), callback);
            this._addChoice(new Bishop(this.board, isOpponent), callback);
            this._addChoice(new Knight(this.board, isOpponent), callback);
            this._addChoice(new Rook(this.board, isOpponent), callback);

            this.element.classList.remove('is-hidden');
        }

        private _addChoice (piece: Piece, callback: (promoted: Piece)=>any)
        {
            let element = document.createElement('div');
            element.classList.add('choice');
            element.appendChild(piece.element);
            element.addEventListener('click', () => {
                callback(piece);
                this.hide();
            });
            this.element.appendChild(element);
        }

        hide ()
        {
            if (!this.element)
                return;

            this.element.classList.add('is-hidden');
            this._clear();
        }

        private _clear () {
            while (this.element.firstChild) {
                this.element.removeChild(this.element.firstChild);
            }
        }

        private _buildElement ()
        {
            if (this.element)
                return;
            
            this.element = document.createElement('div');
            this.element.classList.add('promotion', 'is-hidden');
            this.board.element.appendChild(this.element);
        }
    }
}
