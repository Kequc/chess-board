namespace Chess
{
    export class King extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2654' : '\u265A');
        }

        value (): number
        {
            return 0;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let directions = [Direction.TL, Direction.T, Direction.TR, Direction.R, Direction.BR, Direction.B, Direction.BL, Direction.L];
            let position: number;

            for (let direction of directions) {
                position = State.validDirection(this, direction);
                if (position != null)
                    result.push(position);
            }

            // maybe can castle
            position = this._validCastle(Direction.L);
            if (position != null)
                result.push(position);
            position = this._validCastle(Direction.R);
            if (position != null)
                result.push(position);

            return result;
        }

        move (position: number)
        {
            let col = gridCol(position) - gridCol(this.position);
            if (Math.abs(col) == 2) {
                // moving king two spaces
                let direction = (col < 0 ? Direction.L : Direction.R);
                let castle = this._castle(direction);
                if (castle) {
                    direction = (col > 0 ? Direction.L : Direction.R);
                    let stop = gridIndexDirection(position, direction);
                    let changes: IChange[] = [{
                        start: castle.position,
                        stop: stop,
                        piece: castle,
                        kind: 'castle'
                    }];
                    castle.setPosition(stop);
                    super.move(position, changes);
                    return;
                }
            }
            super.move(position);
        }

        validMoves (): number[]
        {
            let col = gridCol(this.position);

            return this.validPositions().filter((position) => {
                let c = gridCol(position);

                if (Math.abs(c - col) == 2) {
                    // trying to castle
                    if (State.isCheck(this.board, this.isOpponent))
                        return false;

                    let castle = gridIndexOffset(this.position, 0, (c - col) / 2);
                    if (!this.isValidMove(castle))
                        return false;
                }

                return this.isValidMove(position);
            });
        }

        private _castle (direction: Direction): Piece
        {
            if (this.isMoved())
                return;

            let col: number;
            switch (direction) {
                case Direction.L: col = (this.board.isReversed ? -3 : -4); break;
                case Direction.R: col = (this.board.isReversed ? 4 : 3); break;
                default: return;
            }

            let position = gridIndexOffset(this.position, 0, col);
            let rook = State.pieceAt(this.board, position);

            if (rook && !rook.isMoved())
                return rook;
        }

        private _validCastle (direction: Direction): number
        {
            let rook = this._castle(direction);
            if (!rook)
                return;

            let cols: number[];
            switch (direction) {
                case Direction.L: cols = (this.board.isReversed ? [-1,-2] : [-1,-2,-3]); break;
                case Direction.R: cols = (this.board.isReversed ? [1,2,3] : [1,2]); break;
                default: return;
            }

            for (let col of cols) {
                let position = gridIndexOffset(this.position, 0, col);
                if (State.pieceAt(this.board, position))
                    return;
            }

            return gridIndexDirection(this.position, direction, 2);
        }
    }
}
