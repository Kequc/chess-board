namespace Chess
{
    export class Queen extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2655' : '\u265B');
        }

        value (): number
        {
            return 9;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let directions = [Direction.TL, Direction.T, Direction.TR, Direction.R, Direction.BR, Direction.B, Direction.BL, Direction.L];

            for (let direction of directions) {
                result = result.concat(State.validDirectionFull(this, direction));
            }

            return result;
        }
    }
}
