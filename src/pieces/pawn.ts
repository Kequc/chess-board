namespace Chess
{
    export class Pawn extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2659' : '\u265F');
        }

        value (): number
        {
            return 1;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let direction = (this.isOpponent ? Direction.B : Direction.T);

            let position = gridIndexDirection(this.position, direction);
            if (position != null && !State.pieceAt(this.board, position)) {

                result.push(position);

                if (!this.isMoved()) {
                    // maybe can move two squares
                    position = gridIndexDirection(this.position, direction, 2);
                    if (position != null && !State.pieceAt(this.board, position))
                        result.push(position);
                }

            }

            // maybe can capture
            position = this._validCapture(-1);
            if (position != null)
                result.push(position);
            position = this._validCapture(1);
            if (position != null)
                result.push(position);

            return result;
        }

        move (position: number)
        {
            let row = gridRow(position);
            if (row == 0 || row == 7) {
                this._performPromotion(position);
                return;
            }

            let col = gridCol(position) - gridCol(this.position);
            if (Math.abs(col) == 1) {
                let passant = this._passant(col);
                if (passant) {
                    let changes: IChange[] = [{
                        start: passant.position,
                        stop: -1,
                        piece: passant,
                        kind: 'passant'
                    }];
                    passant.remove();
                    super.move(position, changes);
                    return;
                }
            }

            super.move(position);
        }

        private _performPromotion (position: number)
        {
            let taken = State.pieceAt(this.board, position);
            if (taken)
                taken.setVisiblePosition(-1);
            this.setVisiblePosition(position);

            this.board.promotion.show(this.isOpponent, (promoted: Piece) => {
                let changes: IChange[] = [{
                    start: this.position,
                    stop: -1,
                    piece: this,
                    kind: 'promoted'
                }];
                this.remove();
                promoted.move(position, changes);
            });
        }

        private _passant (col: number): Piece
        {
            // could be en passant
            let lastMove = this.board.history.last();
            if (!lastMove || !(lastMove.changes[0].piece instanceof Pawn) || lastMove.changes[0].piece.isOpponent == this.isOpponent)
                return;
            
            if (Math.abs(gridRow(lastMove.changes[0].start) - gridRow(lastMove.changes[0].stop)) == 2) {
                // their pawn moved two spaces
                let position = gridIndexOffset(this.position, 0, col);
                let occupied = State.pieceAt(this.board, position);
                if (occupied && lastMove.changes[0].piece == occupied)
                	return occupied;
            }
        }

        private _validCapture (col: number): number
        {
            let row = (this.isOpponent ? 1 : -1);
            let position = gridIndexOffset(this.position, row, col);

            let occupied = State.pieceAt(this.board, position);
            if (occupied && occupied.isOpponent != this.isOpponent)
                return position;
            
            let passant = this._passant(col);
            if (passant)
                return position;
        }
    }
}
