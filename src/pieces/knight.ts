namespace Chess
{
    export class Knight extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2658' : '\u265E');
        }

        value (): number
        {
            return 3;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let offsets = [[-2,-1],[-2,1],[-1,2],[1,2],[2,-1],[2,1],[-1,-2],[1,-2]];

            for (let offset of offsets) {
                let index = State.validOffset(this, offset[0], offset[1]);
                if (index != null)
                    result.push(index);
            }

            return result;
        }
    }
}
