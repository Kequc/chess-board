namespace Chess
{
    export class Rook extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2656' : '\u265C');
        }

        value (): number
        {
            return 5;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let directions = [Direction.T, Direction.R, Direction.B, Direction.L];

            for (let direction of directions) {
                result = result.concat(State.validDirectionFull(this, direction));
            }

            return result;
        }
    }
}
