namespace Chess
{
    export class Bishop extends Piece
    {
        hex (): string
        {
            return (this.isWhite() ? '\u2657' : '\u265D');
        }

        value (): number
        {
            return 3;
        }

        validPositions (): number[]
        {
            if (this.position == -1)
                return [];

            let result: number[] = [];
            let directions = [Direction.TL, Direction.TR, Direction.BR, Direction.BL];

            for (let direction of directions) {
                result = result.concat(State.validDirectionFull(this, direction));
            }

            return result;
        }
    }
}
