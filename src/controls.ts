namespace Chess.Controls
{
    var _selected: Piece;
    var _validMoves: number[];
    var _dragIndex: number;

    export function unhover (board: Board)
    {
        // remove hover effect
        if (_dragIndex != null)
            board.squares[_dragIndex].classList.remove('is-hovered');

        _dragIndex = undefined;
    }

    export function dragStart (piece: Piece)
    {
        // started dragging
        if (!piece)
            return;

        piece.board.element.classList.add('is-dragging');
        piece.board.squares[piece.position].classList.add('is-start');

        _validMoves = piece.validMoves();

        for (let i = 0; i < piece.board.squares.length; i++) {
            let square = piece.board.squares[i];
            let isValid = _validMoves.indexOf(i) > -1;
            square.classList.toggle('is-valid', isValid);
        }

        _selected = piece;
    }

    export function dragEnd (board: Board)
    {
        // finished dragging
        board.element.classList.remove('is-dragging');

        if (_selected) {
            let square = board.squares[_selected.position]
            if (square)
                square.classList.remove('is-start');

            if (_validMoves.indexOf(_dragIndex) > -1) {
                _selected.move(_dragIndex);
                _selected.board.setTurn();
            }
        }

        unhover(board);
        _selected = undefined;
        _validMoves = undefined;
    }

    export function dragEnter (board: Board, index: number)
    {
        // hovered over square during dragging
        unhover(board);
        _dragIndex = index;
        board.squares[_dragIndex].classList.add('is-hovered');
    }
}
